var gulp = require('gulp');
var path = require('path');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');
var jsonlint = require('gulp-jsonlint');
var merge = require('merge-stream');
var minifyHTML = require('gulp-minify-html');

// images
var imagemin = require('gulp-imagemin');

// scripts
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');

// styles
var minifyCss = require('gulp-minify-css');

var debug = require('gulp-debug');
/* 
 * usage: 
 * .pipe(debug())
 */

var paths = {
  images: 'src/images/*',
  scripts: 'src/scripts/*.js',
  styles:'src/styles/*.css',
  manifest:'build/manifest.json'
};

gulp.task('jsonlint',function(){
  return gulp.src(paths.manifest)
    .pipe(jsonlint())
    .pipe(jsonlint.reporter())
});

gulp.task('jshint',function(){
  return gulp.src(paths.scripts)
    .pipe(jshint())
    .pipe(jshint.reporter())
});

gulp.task('js', function() {
  // Minify and copy all JavaScript (except vendor scripts) 
  // with sourcemaps all the way down 
  var js = gulp.src(paths.scripts)
    .pipe(jshint())
    .pipe(sourcemaps.init())
    .pipe(uglify())
    // .pipe(concat('scripts.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/js'));

  var vendor = gulp.src('src/scripts/vendor/*.js')
    .pipe(gulp.dest('build/js/vendor'));
  
  return merge(js, vendor);
});
 
gulp.task('minify-html', function() {
  var opts = {
    conditionals: true,
    spare:true
  };
  
  var page = gulp.src('src/pages/*.html')
    .pipe(minifyHTML(opts))
    .pipe(gulp.dest('build/pages'));

  var partials = gulp.src('src/pages/partials*.html')
    .pipe(minifyHTML(opts))
    .pipe(gulp.dest('build/pages/partials'))

  return merge(page, partials);
});

// Copy all static images 
gulp.task('images', function() {
  return gulp.src(paths.images)
    // Pass in options to the task 
    .pipe(imagemin({optimizationLevel: 5}))
    .pipe(gulp.dest('build/images'));
});
 
  
gulp.task('css', function () {
  return gulp.src(paths.styles)
    .pipe(sourcemaps.init())
    .pipe(minifyCss())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/css'));
});
 
// Rerun the task when a file changes 
gulp.task('watch', function() {
  gulp.watch(paths.scripts, ['js']);
  gulp.watch(['src/pages/*.html','src/pages/partials*.html'], ['minify-html']);
  gulp.watch(paths.styles, ['css']);
  gulp.watch(paths.images, ['images']);
  gulp.watch(paths.manifest, ['jsonlint']);
});
 
// The default task (called when you run `gulp` from cli) 
gulp.task('default', ['watch', 'jsonlint','js', 'images','css', 'minify-html']);