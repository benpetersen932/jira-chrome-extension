var commentsApp = angular.module('commentsApp', ['ngRoute'])
	.config(['$compileProvider', '$routeProvider', function($compileProvider, $routeProvider) {
			//href whitelist
			$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|chrome-extension|sip):/);
			//routes
			$routeProvider
				.when('/details', {
					controller:'DetailsCtrl',
					templateUrl: '../pages/partials/details.html'
				})
				.when('/dates', {
					controller:'DatesCtrl',
					templateUrl: '../pages/partials/dates.html'
				})
				.when('/people', {
					controller:'PeopleCtrl',
					templateUrl: '../pages/partials/people.html'
				})
				.when('/attachments', {
					controller:'AttachmentsCtrl',
					templateUrl: '../pages/partials/attachments.html'
				})
				.otherwise({
					redirectTo: '../pages/partials/details.html'
				});
		}]);

commentsApp		
	.service('issueService', ['$http',function ($http) {
		// converts query string into object
		var toObject = function(s){
				var	obj = {}, pair = null, parts = s.split('&');

				for (var i = 0; i < parts.length; i++){
				    pair = parts[i].split('=');
				    obj[pair[0]] = pair[1];
				};

				return obj;
			},
			params = toObject(atob(document.location.search.substr(1))),
			url = params.domain + 'rest/api/2/issue/' + params.id,
			_promise;

		return {
			async: function(noCache){
				if(!_promise || noCache){
					_promise = $http.get(url);
				}

				return _promise;
			},
			baseUrl:params.domain,
			id: params.id
		}
	}])
	.service('transitionsService', ['$http',function ($http) {
		// converts query string into object
		var toObject = function(s){
				var	obj = {}, pair = null, parts = s.split('&');

				for (var i = 0; i < parts.length; i++){
				    pair = parts[i].split('=');
				    obj[pair[0]] = pair[1];
				};

				return obj;
			},
			params = toObject(atob(document.location.search.substr(1))),
			url = params.domain + 'rest/api/2/issue/' + params.id + '/transitions';

		return {
			async: function(clearCache){
				return $http.get(url);
			},
			baseUrl:params.domain,
			id: params.id
		}
	}])
	.service('userService',['$http',function($http){
		return {
			get:function(domain, user){
				var url = domain + 'rest/api/2/user/search?username=' + user
				return $http.get(url);
			}
		};
	}])
	.controller('CommentsCtrl', ['$scope', '$http', '$location', '$filter', '$timeout','$interval', 'issueService','transitionsService', function ($scope, $http, $location, $filter, $timeout, $interval, issueService, transitionsService) {
		// converts query string into object
		var	path = $location.path();

		$scope.baseUrl = issueService.baseUrl;
		$scope.id = issueService.id;
		$scope.loading = 'loading';
		$scope.sections = [
			//details
			{
				icon:'option-horizontal',
				name:'details'
			},
			//people
			{
				icon:'user',
				name:'people'
			},
			//time
			{
				icon:'time',
				name:'dates'
			},
			//attachements
			{
				icon:'picture',
				name:'attachments'
			}
		];

		$scope.tab = path.substr(1);
		$scope.user = chrome.extension.getBackgroundPage().AppManager.user;

		if(!path){
			path = $scope.sections[0].name;
			$scope.tab = path;
			$location.path(path);
		}

		$scope.abbrevate = function(name){
			return name.match(/[A-Z]+/g).join('');
		}

		$scope.changeTab = function(section){
			$scope.tab = section.name;
		}

		$scope.comment = function(){
			if(!!$scope.body.length){
				var url = $scope.baseUrl + 'rest/api/2/issue/' + $scope.id + '/comment',
					comment = { body: $scope.body };
				
				$scope.loading = 'loading';

				if($scope.reassign && !!$scope.reassignee){
					$scope.reassignIssue();
				}

				//post comment
				$http.post(url, comment)
					.success(function(data, status, headers, config) {
						$scope.body = '';
						$scope.getComments();
					})
					.error(function(data, status, headers, config) {
						console.log(data);
					});
			}
		}

		$scope.clickAssignee = function(user){
			$scope.reassignee = user;
			$scope.reassignees = null;
		}

		$scope.getAvatar = function(author){
			var avatar = author.avatarUrls['48x48'],
				//todo: use people api photos. currently that call is too slow to create a "smooth" effect
				isCustom = avatar.indexOf(author.name) > - 1,
				peopleApi = 'https://peopleapi.intranet.wsod.local/People/GetPicture?sAMAccountName=' + author.name;

			return avatar;
		}

		$scope.getComments = function(){
			var url = $scope.baseUrl + 'rest/api/2/issue/' + $scope.id;

			$http.get(url)
				.success(function(data, status, headers, config) {
					console.log(data);

					$scope.comments = $filter('comments')(data.fields.comment.comments);
					$scope.issue = data;
					$scope.loading = '';
				})
				.error(function(data, status, headers, config) {
				
				});
		}

		$scope.getTransitions = function(){
			transitionsService.async().then(function(response){
				$scope.transitions = response.data.transitions;
			});
		}

		var timer = false;
		$scope.reassignKeyup = function($event){
			var value = $event.target.value;

			if(value.length > 2){
				if (timer) $timeout.cancel(timer);

				var url = $scope.baseUrl + 'rest/api/2/groupuserpicker?maxResults=5&query=' + value;
				timer = $timeout(function(){
					$http.get(url)
						.success(function(data, status, headers, config){
							$scope.reassignees = data.users.users;
						});
				}, 500);
			}
			else{
				$scope.reassignees = null;
			}
		}

		$scope.reassignIssue = function(){
			if($scope.reassign && !!$scope.reassignee){
				var url = $scope.baseUrl + 'rest/api/2/issue/' + $scope.id,
					data = {
						'fields': {
							'assignee':{ 'name': $scope.reassignee.name}
						}
					};

				// reassign 
				$http.put(url,data)
					.success(function(data, status, headers, config) {
						// success = 204 / no content
						$scope.reassign = false;
						$scope.reassignee - null;
					})
					.error(function(data, status, headers, config) {
						console.error('jira chrome extension:', data);
					});
			}
		}

		$scope.transitionIssue = function(id){
			if(!!typeof id){
				var url = $scope.baseUrl + 'rest/api/2/issue/' + $scope.id + '/transitions',
					data = {
						'transition':{
							'id':id
						}
					};

				if($scope.reassign && !!$scope.reassignee){
					data['fields'] = {
						'assignee':{ 'name': $scope.reassignee.name}
					};
				}

				// transition issue 
				$http.post(url,data)
					.success(function(data, status, headers, config) {
						// success = 204 / no content
						$scope.comment();
						$scope.getTransitions();
					})
					.error(function(data, status, headers, config) {
						console.log(data);
					});
			}
		}

		var updateComments = function(response){
			$scope.comments = $filter('comments')(response.data.fields.comment.comments);
			$scope.issue = response.data;
			$scope.loading = '';

			$scope.created = response.data.fields.created;

			$scope.getTransitions();
		}

		issueService.async(true).then(function(response){
			updateComments(response);

			var ONE_MIN = 60000;
			// start the interval
			$interval(function(){
				issueService.async(true).then(updateComments);
			}, ONE_MIN);
		});
	}])
	// attachments
	.controller('AttachmentsCtrl',['$scope','issueService', function($scope, issueService){
		issueService.async().then(function(response){
			$scope.attachments = response.data.fields.attachment;
		});
	}])
	// dates
	.controller('DatesCtrl',['$scope','issueService','$filter', function($scope, issueService, $filter){
		issueService.async().then(function(response){
			var issue = response.data.fields,
				comments = issue.comment.comments,
				isEmpty = $filter('isEmptyObject'),
				sort = $filter('arraySort');

			$scope.lastViewed = issue.lastViewed;
			$scope.resolution = issue.resolution;
			$scope.timeline = [
				{
					action:'Created',
					actor:issue.reporter,
					date:issue.created

				},
				{
					action:'Updated',
					actor:comments.length
						? comments[comments.length - 1].updateAuthor
						: null,
					date:issue.updated,
					icon: comments.length
						? null
						: 'repeat'
				},
				{
					actor: !!issue.resolutiondate 
						? null 
						: issue.assignee,
					action:!!issue.resolution
						? issue.resolution.name 
						: issue.status.name,
					date:issue.resolutiondate,
					icon: !!issue.resolution ? 'ok' : null
				}
			].sort(sort);

			if(!!issue.timetracking && !isEmpty(issue.timetracking)){
				$scope.timetracking = angular.extend({
					timeestimate:issue.timeestimate,
					timeoriginalestimate:issue.timeoriginalestimate,
					timespent:issue.timespent
				}, issue.timetracking);

				console.log($scope.timetracking);
			}
		});
	}])
	// details
	.controller('DetailsCtrl',['$scope','issueService', function($scope, issueService){
		issueService.async().then(function(response){
			var issue = response.data.fields;

			//TODO: cleanup
			$scope.description = issue.description;
			$scope.fixVersions = issue.fixVersions;
			$scope.issuelinks = issue.issuelinks.map(function(issue,i){
				if(issue.inwardIssue){
					issue.inwardIssue['type'] = 'inward';
					return issue.inwardIssue;
				}
				if(issue.outwardIssue){
					issue.outwardIssue['type'] = 'outward';
					return issue.outwardIssue;
				}


			});
			$scope.issuetype = issue.issuetype;
			$scope.labels = issue.labels;
			$scope.priority = issue.priority;
			$scope.project = issue.project;
			$scope.resolution = issue.resolution;
			$scope.subtasks = issue.subtasks;
			$scope.status = issue.status;

			$scope.issueUrl = function(key){
				return '?' + btoa('domain=' + issueService.baseUrl + '&id=' + key);
			}
		});
	}])
	// people
	.controller('PeopleCtrl',['$scope','$http','issueService', function($scope, $http, issueService){
		issueService.async().then(function(response){
			var issue = response.data.fields,
				comments = issue.comment.comments;

			$scope.assignee = issue.assignee;
			$scope.reporter = issue.reporter;
			$scope.commenters = comments.map(function(comment,i){
				return comment.author;
			})

			// watches
			$http.get(issue.watches.self)
				.success(function(data, status, headers, config){
					$scope.watches = data;
				});
		});

		$scope.hasCommented = function(user){
			var name = user.displayName;

			for(var i = 0; i < $scope.comments.length; i++){
				var comment = $scope.comments[i],
					author = comment.author;

				if(name === author.displayName){
					return true;
				}
			}

			return false;
		}

		$scope.getComments = function(user){
			var name = user.displayName,
				comments = [];

			for(var i = 0; i < $scope.comments.length; i++){
				var comment = $scope.comments[i],
					author = comment.author;

				if(name === author.displayName){
					comments.push(comment);
				}
			}

			return comments;
		}
	}])
	.directive('scrollToLast', ['$timeout', function ($timeout) {
		return {
			scope: {
		  		scrollBottom: "="
			},
			link: function ($scope, element, attrs) {
				// scroll to the last (most recent item)
				if($scope.$parent.$last){
					// w/out timeout, margin/padding isn't taken into account
					$timeout(function(){
						element[0].scrollIntoView();
					},0);
				}
				// $scope.$watchCollection('comments', function (newValue) {
				// 	if (newValue){
				// 		element[0].scrollIntoView();
				// 	}
				// 	else if($scope.$parent.$last){
				// 		$timeout(function(){
				// 			element[0].scrollIntoView();
				// 		},0);
				// 	}
			 //    });
			}
		}
	}])
	.filter('abbreviate',['$sce', function($sce){
		return function(name){			
			return name.match(/[A-Z]+/g).join('');
		};	
	}])
	.filter('comments',['$sce', 'issueService', 'userService', function($sce, issueService, userService){
		return function(comments){
			if(!comments){ return comments; }

			// var emailTildaPat = /~([\w\.]+)@([\w\.]+)\.(\w+)/g,
			var emailTildaPat = /~([\w\.]+)(@([\w\.]+)\.(\w+))?/g,
				// fullUsernamePat = /(\[~([\w\.]+)@([\w\.]+)\.(\w+)\])/g;
				fullUsernamePat = /(\[~([\w\.]+)(@([\w\.]+)\.(\w+)\]))?/g;

			for(var i = 0; i < comments.length; i++){
				var comment = comments[i].body;

				if(comment.match(emailTildaPat) && comment.match(fullUsernamePat)){
					var usernames = comment.match(emailTildaPat),
						idx = i;

					for(var j = 0; j < usernames.length; j++){
						var username = usernames[j].substr(1);

						userService.get(issueService.baseUrl, username)
							.then(function(response){
								var user = response.data[0],
									replacer = '[~' + user.name + ']',
									replacee = '<span class="user">' + user.displayName + '</span>';

								comments[idx].body = comments[idx].body.replace(replacer, replacee);
							});
					}
				}
			}

			return comments;
		};	
	}])
	.filter('formatMarkdown',['$sce',function($sce){
		return function(s){			
			var textPat = /(?:\[)(.*)(?=\|)/g,
				linkPat = /(?:\|)(.*)(?=\])/g;

			if(!s){
				return s;
			}

			// format: [text|link]
			if(!!s && s.match(textPat) && s.match(linkPat)){
				var text = s.match(textPat)[0].substr(1);

				var link = s.match(linkPat)[0];
				while(link.indexOf(']') > -1){
					link = link.match(linkPat)[0];
				}

				link = link.substr(1);

				s = s
					.replace(text,'~')
					.replace(link,'~')
					.replace('[~|~]','<a href="' + link + '" target="top">' + text + '</a>');
			}

			// newlines to <br>
			if(!!s && s.indexOf('\n') > -1){
				s = s.split('\n').join('<br>');
			}

			// urls to links
			// var url = /(http(s)?:\/\/[a-zA-Z0-9\-_]+\.[a-zA-Z]+(.)+)+/gm;
			var url = /(http(s)?:\/\/[a-zA-Z0-9\-_]+\.[a-zA-Z]+([a-zA-Z0-9\?\=\-\&\.\:\_/])+)+/g;
			if(s.match(url)){
				var matches = s.match(url);

				for(var m = 0; m < matches.length; m++){
					s = s.replace(matches[m], '<a href="' + matches[m] + '" target="_blank">' + matches[m] + '</a>');
				}
			}

			var bold = /\*\b([\w\s\&\\\/]+)\*\B/g;
			if(s.match(bold)){
				s = s.replace(bold,'<strong>\$1</strong>');
			}


			var email = /([\w\.]+)@([\w\.]+)\.(\w+)/g,
				fullmailto = /\[mailto:([\w\.]+)@([\w\.]+)\.(\w+)\]/g;

			if(s.match(fullmailto)){
				var matches = s.match(fullmailto);

				for(var m = 0; m < matches.length; m++){
					var match = matches[m].match(email)[0];
					s = s.replace(matches[m], '<a href="mailto:' + match + '" target="_blank">' + match + '</a>');
				}
			}

			// todo:
			// format: [text](link)

			return $sce.trustAsHtml(s);
		}
	}])
	.filter('formatBytes', function(){
		return function(bytes) {
		   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
		   if (bytes == 0) return '0 Byte';
		   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
		}
	})
	.filter('formatDate', function(){
		return function(date){
			return moment(date).format('MMM D, YYYY h:mm a');
		};	
	})
	.filter('fromNow', function(){
		return function(date){
			return moment(date).fromNow();;
		};	
	})
	.filter('isEmptyObject', function(){
		return function (obj) {
			for(var key in obj) {
				if(obj.hasOwnProperty(key)){
					return false;
				}
			}
			return true;
		};
	})
	.filter('arraySort', function(){
		return function (a,b) {
			if(a.date < b.date) return -1;
			if(a.date > b.date) return 1;
			return 0;
		};
	});