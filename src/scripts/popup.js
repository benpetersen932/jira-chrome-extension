// TODO: popout window
// chrome.extension.getViews({ type: "popup" });
// chrome.extension.getViews({ type: "panel" });

var PopupManager = function() {
	this._init();
};

PopupManager.prototype = {
	_init: function() {
		this.app = chrome.extension.getBackgroundPage().app;
		this.$container = $('#container');
		
		this.markitJira = new Jira({
			baseUrl:'https://jira.markit.com/',
			container:'#markit-jira',
			issues:this.app.issues.jira,
			total:this.app.issues.jiraTotal
		});

		this.markitProjects = new Jira({
			baseUrl:'https://projects.markit.com/',
			container:'#markit-projects',
			issues:this.app.issues.projects,
			total:this.app.issues.projectsTotal
		});

		var updateTextNode = function(el, text){
			var $text = $(el)
				.contents()
				.filter(function(){ 
					return this.nodeType === 3 && !!$.trim(this.nodeValue);
				}).replaceWith(text);
		};

		updateTextNode('a[href="#markit-jira"]', 'Markit Jira (' + this.app.totals.jira + ')');
		updateTextNode('a[href="#markit-projects"]', 'Markit Projects (' + this.app.totals.projects + ')');

		this._attachEvents();

		var isPanel = !chrome.extension.getViews({ type: "popup" }).length;

		if(isPanel){
			this._initPanel();
		}

		var product = this.app.storage.get('product');
		if(!!product){
			$('a[href="' + product + '"]').trigger('click');
		}
	},
	
	_initPanel:function(){
		var search = this.app.storage.get('search');

		if(!!search){
			this.app.storage.remove('search');

			$('#search').val(search);
			$('#form').trigger('submit');
		}

		$('#popout').hide();
		
		this.$container
			.addClass('panel');
	},

	_attachEvents: function() {
		this.$container
			.on('click','a[href^="http"]',$.proxy(function(e){
				e.preventDefault();
				var $target = $(e.currentTarget),
					href = $target.attr('href');

				if($target.is('#jira-link')){
					var jql = this.$container.find('.search-list:visible').data('jql') || this.app.getQuery();
					href += '?jql=' + encodeURIComponent(jql);
				}

				chrome.tabs.create({ url: href });
			},this))
			.on('click','.dropdown-menu a',$.proxy(function(e){
				var $target = $(e.currentTarget),
					$parent = $target.parent('li'),
					href = $target.attr('href'),
					html = $target.html();

				if($parent.hasClass('.active')){
					return;
				}

				$('#jira-link')
					.attr('href', $target.data('href'))
					.html(html);

				$parent
					.addClass('active')
					.siblings().removeClass('active');

				$(href)
					.show()
					.siblings('section[id]').hide();

				this.app.storage.set('product', href);
			},this));
		
		$('#popout').on('click',$.proxy(function(){
			var search = $('#search').val();

			if(!!search){
				this.app.storage.set('search',search);
			}

			chrome.windows.create({
				height:600,
				url:chrome.extension.getURL('/pages/popup.html'),
				type:'panel',
				width:350
			},function(){
				chrome.extension.getViews({ type: "popup" })[0].close();
			});
		},this));

		$('#btn-refresh').on('click',$.proxy(function(){
			document.location.reload();
			// var product = this.app.storage.get('product');

			// if(product === '#markit-jira'){
			// 	this.markitJira.getIssues();
			// }
			// else{
			// 	this.markitProjects.getIssues();
			// }
		},this));
	}
};

(function(w, d) {
	w.popup = new PopupManager();
})(window, document);
