/*
 * baseUrl
 * container
 * issues
 * total
 */
var Jira = function(config){
	if(this._validateConfig(config)){
		this._init(config);
	}
};

Jira.prototype = {
	_init:function(config){
		this.app = chrome.extension.getBackgroundPage().app
		this.BASE_URL = config.baseUrl;
		this.$container = $(config.container);
		this.issues = [];

		if(!!config.issues.length){
			var issues = config.issues;
			$('body').removeClass('loading');

			this._renderIssues(issues);

			if(config.total > issues.length){
				this._renderShowMore(issues.length + 1);
			}

			this._lookupProjects(issues);
			this._getWatchers(issues);
			this.issues = this.issues.concat(issues);
		}
		else{
			this.getIssues();
		}

		this._attachEvents();
	},

	_validateConfig:function(config){
		var isValid = true,
			required = ['baseUrl', 'container'];

		config = $.extend({},config);

		for(var i = 0; i < required.length; i++){
			var	prop = required[i];
			if(!config.hasOwnProperty(prop)){
				console.error('Missing required "' + prop +'".')
				isValid = false;
			}
		}

		return isValid;
	},

	_attachEvents: function() {
		this.$container
			.on('click','.btn-comment', $.proxy(this._handleClickComment, this))
			.on('click','.btn-show-more', $.proxy(this._handleClickShowMore, this))
			.on('submit','form',$.proxy(this._handleSearch,this));
	},

	_lookupProjects:function(issues){
		var projects = {};
		issues = issues || this.issues;

		if(issues){
			var self = this;
			$.each(issues,function(i,issue){
				var key = issue.key,
					project = key.split('-')[0];

				issue['project'] = project;

				if(!projects[project]){

					$.ajax({
						context:self,
						error:function(xhr, status, err){
							console.warn(project, status);
						},
						success:function(data, status, xhr){
							projects[project] = data;
							issue['project'] = data;
						},
						url: self.BASE_URL + 'rest/api/2/project/' + project
					});
				}
			});
		}
	},

	_getWatchers:function(issues){
		var watchers = {};
		issues = issues || this.issues;

		if(issues){
			var self = this;
			$.each(issues,function(i,issue){
				var key = issue.key,
					url = issue.self + '/watchers';

				issue['watchers'] = { isWatching: false };

				if(!watchers[key]){
					$.ajax({
						context:self,
						error:function(xhr, status, err){
							console.warn(project, status);
						},
						success:function(data, status, xhr){
							watchers[key] = data;
							issue['watchers'] = data;
						},
						url: url
					});
				}
			});
		}
	},

	_handleClickComment:function(e){
		var $btn = $(e.currentTarget),
			id = $btn.data('id'),
			domain = this.BASE_URL,
			params = 'domain=' + domain + '&id=' + id,
			enc = btoa(params);

		chrome.windows.create({
			height:450,
			type:'panel',
			url:chrome.extension.getURL('/pages/comments.html?' + enc),
			width:375,
		});
	},

	_handleClickShowMore:function(e){
		var $btn = $(e.currentTarget),
			$container = $btn.parents('section[data-list]'),
			startAt = $btn.data('start-at');

		this.getIssues({
			jql: $container.data('jql'),
			renderTo: ' .' + $container.data('list'),
			startAt: startAt
		});
	},

	_handleSearch:function(e){
		e.preventDefault();

		var $form = $(e.currentTarget),
			$searchList = this.$container.find('.search-list'),
			search = $form.find('input').val(),
			jql = 'text ~ "' + search + '" ORDER BY created DESC',
			lastSearch = $searchList.data('jql');

		$searchList
			.data('jql', jql)
			.empty()
			.append('<ul class="issues"></ul>');		

		if(!!search){
			this.getIssues({
				jql:jql,
				renderTo:' .search-list'
			}).then(function(data,xhr,text){
				console.log(data);
				if(data.total > 0){
					$searchList.find('> ul').attr('data-label','Search for "' + search + '" (' + data.total + ' results)');
				}
			});
		}
	},

	_renderIssues:function(issues, renderTo){
		var selector = (renderTo || ' .default-list') + '> ul';
			$ul = this.$container.find(selector);

		$.each(issues, $.proxy(function(i, issue){
			var li = this._renderIssue(issue);

			$ul.append(li);
		},this));
	},

	_renderIssue:function(issue){
		var key = issue.key,
			summary = $.trim(issue.fields.summary),
			apiUrl = issue.self
			url = this.BASE_URL + 'browse/' + key,
			issuetype = issue.fields.issuetype,
			created = issue.fields.created,
			assignee = issue.fields.assignee
			user = this.app.user,
			isWatching = !!issue.watchers && issue.watchers.isWatching;

		var renderField = function(field){
			return !!field
				? ['<li><img alt="', field.name, '" title="', field.name, '" src="' ,field.iconUrl,'" no-repeat" /></li>'].join('')
				: '';
		};

		var ownership = !!assignee && 
			(assignee.name === user.jira.username || assignee.name === user.projects.username)
				? ' owner'
				: ''

		var statusColor = !!issue.fields.status.statusCategory
			? issue.fields.status.statusCategory.colorName
			: 'primary';

		return [
			'<li data-is-watching="', isWatching ,'">',
				'<ul class="nav">',
					//renderField(issue.fields.status),
					//renderField(issue.fields.priority),
					'<li>',
						'<button data-comments="', issue.fields.comment.comments.length,'" data-id="', key ,'"class="btn btn-link btn-lg btn-comment glyphicon glyphicon-comment"></button>',
					'</li>',
				'</ul>',
				'<div class="issue-links',ownership,'">',
					'<a class="issue-title" href="',url,'">',
						'<img alt="', issuetype.name, '" src="', issuetype.iconUrl, '" title="', issuetype.name,'" />',
						key,
					'</a>',
					'<span class="label label-', statusColor, '">',issue.fields.status.name,'</span>',
					'<a href="',url,'" title="', summary ,'">',summary,'</a>',
					'<time datetime=',  created ,'>', moment(created).fromNow() ,'</time>',
				'</div>',
			'</li>'
		].join('');
	},

	_renderShowMore:function(startAt, renderTo){
		var selector = renderTo || ' .default-list'
			$btn = this.$container.find(selector + ' .btn-show-more');

		if(!!$btn.length){
			$btn.data('start-at', startAt);
		}
		else{
			$btn = $('<button data-start-at="' + startAt + '" class="btn btn-block btn-primary btn-sm btn-show-more">Show More</button>')
				.appendTo(this.$container.find(selector));
		}
	},

	getIssues:function(options){
		
		options = $.extend({
			jql: this.app.getQuery(),
			maxResults:10,
			renderTo: ' .default-list',
			startAt: 0
		}, options);

		return this.app.$getCurrentIssues({
			beforeSend:function(){
				this.$container.addClass('loading');
			},
			complete:function(){
				$('body').removeClass('loading');
				this.$container.removeClass('loading');
			},
			context:this,
			data:{
				fields: 'id,key,summary,comment,issuetype,created,assignee,status',
				jql:options.jql,
				maxResults:options.maxResults,
				startAt:options.startAt
			},
			success:function(data,text,xhr){
				if(data.issues.length){
					this._lookupProjects(data.issues);
					this._getWatchers(data.issues);

					this.issues = this.issues.concat(data.issues);
				}

				if(data.total > 0){
					this._renderIssues(data.issues, options.renderTo);

					if(data.issues.length + data.startAt < data.total){
						this._renderShowMore(data.issues.length + data.startAt, options.renderTo);
					}
					else{
						this.$container.find(options.renderTo + ' .btn-show-more').remove();	
					}
				}
				else{
					if(options.renderTo.indexOf('default') > -1){
						this.$container.find(options.renderTo + ' ul')
							.append('<li class="no-data">No issues currently assigned.</li>');
					}
				}
			},
			url:this.BASE_URL + 'rest/api/2/search'
		});
	}
};