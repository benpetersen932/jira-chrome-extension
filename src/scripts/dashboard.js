/* global angular */

var dashboardApp = angular.module('dashboardApp', []);

dashboardApp
	.factory('activityFactory',['$http',function($http){
		var _promise,
			url = 'activity';
			
		return {
			async: function(opts){
				if(!_promise || opts.nocache){
					_promise = $http.get(opts.domain + url);
				}

				return _promise;
			}
		}
	}])
	.factory('authFactory',['$http',function($http){
		var _promise,
			url = 'rest/auth/1/session';
			
		return {
			async: function(opts){
				if(!_promise || opts.nocache){
					_promise = $http.get(opts.domain + url);
				}

				return _promise;
			}
		}
	}])
	.factory('favoritesFactory',['$http',function($http){
		var _promise,
			url = 'rest/api/2/filter/favourite';
		
		return {
			async: function(opts){
				if(!_promise || opts.nocache){
					_promise = $http.get(opts.domain + url);
				}

				return _promise;
			}
		}
	}])
	.factory('issueFactory',['$http',function($http){
		var _promise,
			url = 'rest/api/2/issue/';
		
		return {
			async: function(opts){
				if(!_promise || opts.nocache){
					_promise = $http.get(opts.domain + url + opts.issue);
				}

				return _promise;
			}
		}
	}])
	.factory('searchFactory',['$http',function($http){
		var _promise,
			url = 'rest/api/2/search';
		
		return {
			async: function(opts){
				if(!_promise || opts.nocache){
					_promise = $http.get(opts.domain + url, {
						fields:opts.fields || 'id,key,summary,comment,issuetype,created,assignee',
						jql:opts.jql,
						maxResults:opts.maxResults || 10,
						startAt:opts.startAt || 0
					});
				}

				return _promise;
			}
		}
	}])
	.controller('jiraService',['$http',function($http,$q){
		this.getActivity = function(opts){
			var _promise,
			url = 'rest/api/2/search';
		
			if(!_promise || opts.nocache){
				_promise = $http.get(opts.domain + url, {
					fields:opts.fields || 'id,key,summary,comment,issuetype,created,assignee',
					jql:opts.jql,
					maxResults:opts.maxResults || 10,
					startAt:opts.startAt || 0
				});
			}

			return _promise;
		}
	}])
	.controller('DashboardCtrl', ['$scope', '$http', '$location', function ($scope, $http, $location) {
		var app = chrome.extension.getBackgroundPage().AppManager;
		// get # of items assigned in markit jira
		// get # of items assigned in projects jira
		// $http.get(searchUrl + jql=assignee=currentUser() AND resolution=unresolved)

		// get # of items reported in markit jira
		// get # of items reported in projects jira
		// $http.get(searchUrl + jql=reporter=currentUser() AND resolution=unresolved)

		// get # of watches in markit jira
		// get # of watches in projects jira
		// $http.get({{searchUrl}}jql=watcher=currentUser() AND resolution=unresolved)

		// get markit jira activity
		// get projects jira activity
		// $http.get({{baseUrl}}activity?streams=user+IS+{{user.name}}&maxResults=10)

		$scope.user = app.user;

		$scope.favorites = [];
		$scope.getFavorites = function(){
			
		}

		$scope.getFavorites();
	}]);