/* 
user dashboard:
 - activity (projects & jira)
 - issues reported
 - issues assigned
 - watching
 */

(function ($) {
	var Options = function(){
		this.init()
	};

	Options.prototype.init = function() {
		this.app = chrome.extension.getBackgroundPage().app;
		this._BASE_URL = 'https://jira.markit.com/rest/api/2/search'

		this.$alert = $('#alert');
		this.$btnRemove = $('#btn-remove');
		this.$btnSave = $('#btn-save');
		this.$btnTest = $('#btn-test');
		this.$input = $('#input-jql');

		this._setFromConfig();
		this._retrieveFavorites();
		this.attachEvents();
	};


	Options.prototype.attachEvents = function(){
		this.$alert.on('click','a.close',$.proxy(function(){ this.$alert.hide(); },this));
		this.$btnRemove.on('click',$.proxy(this._handleClickRemove, this));
		this.$btnSave.on('click',$.proxy(this._handleClickSave, this));
		this.$btnTest.on('click',$.proxy(this._handleClickTest, this));
		this.$input.on('keyup',$.proxy(this._handleKeyup, this));

		$('#favorites').on('change',$.proxy(this._handleChangeFavorites,this));
	};

	Options.prototype._setFromConfig = function(){
		var query = this.app.storage.get('query');

		if(!!query){
			this.$btnRemove.parent('.input-group-btn').show();
			this.$input.val(query);
		}
	};
	
	Options.prototype._retrieveFavorites = function(){
		$.ajax({
			context:this,
			error:function(xhr,err,status){
				//hide favorites
				$('#favorites-group').hide();
			},
			success:function(data,status,xhr){
				var $select = $('#favorites');

				if(data.length){
					$.each(data,function(i,favorite){
						var $opt = $('<option/>')
							.attr({ value:favorite.jql })
							.data('viewUrl',favorite.viewUrl)
							.text(favorite.name);

						$opt.appendTo($select);
					});

					$('#favorites-group').fadeIn('fast');
				}
				else{
					//hide favorites
					$('#favorites-group').hide();
				}
			},
			type:'get',
			url:'https://jira.markit.com/rest/api/2/filter/favourite'
		})
	};

	Options.prototype._handleClickRemove = function(e){
		var $btn = $(e.currentTarget);

		this.$input.val('');
		this.$btnSave.attr('disabled','disabled');
		this.$btnTest.attr('disabled','disabled');
		$btn.parent('.input-group-btn').hide();

		this.app.storage.remove('query');
	};

	Options.prototype._handleClickSave = function(e){
		var query = this.$input.val();

		this.app.storage.set('query', query);

		this.$btnRemove.parent('.input-group-btn').show();
		this._showSuccessAlert('<b>Saved!</b> Click "x" to clear saved query.');

		this.$input.trigger('focus');
	};
	
	Options.prototype._handleClickTest = function(e){
		var query = this.$input.val(),
			$well = $('#container .well');

		console.log(query);

		$.ajax({
			beforeSend:function(){
				$well.addClass('loading');
			},
			complete:function(){
				$well.removeClass('loading');
			},
			context:this,
			data:{ jql: query },
			error:function(xhr, err, status){
				var msg = xhr.responseJSON.errorMessages
					|| xhr.responseJSON.warningMessages;

				this._showInvalidQuery(msg);
			},
			success:function(data,status,xhr){
				if(!!data.errorMessages || !!data.warningMessages){
					var msg = data.errorMessages 
						|| data.warningMessages;

					this._showInvalidQuery(msg);
				}
				else{
					this.$alert.hide();

					this.$btnSave.removeAttr('disabled');

					this._showSuccessAlert('<b>Success!</b> Click "Save" to save this query (' + data.total + ' results).');
				}
			},
			type:'get',
			url:this._BASE_URL
		})
	};

	Options.prototype._handleChangeFavorites = function(e){
		var $btnFavorite = $('#btn-favorite'),
			$select = $(e.currentTarget),
			$opt = $select.find(':selected'),
			value = $select.val();

		if(!!value){
			this.$input.val(value);
			this.$btnSave.removeAttr('disabled');
			this.$btnTest.removeAttr('disabled');

			$btnFavorite
				.attr({ 'href': $opt.data('viewUrl') })
				.removeAttr('disabled');
		}
		else{
			this.$input.val('');
			this.$btnSave.attr('disabled','disabled');
			this.$btnTest.attr('disabled','disabled');
			$btnFavorite
				.attr({ 
					disabled:'disabled',
					href:'#'
				});
		}
	};

	Options.prototype._handleKeyup = function(e){
		var $input = $(e.currentTarget),
			val = $input.val().trim();

		if(e.which === 13 && val.length > 0){
			this.$btnTest.trigger('click');
		}

		this.$btnSave.attr('disabled','disabled');

		if(!!val){
			this.$btnTest.removeAttr('disabled');
		}
		else{
			this.$btnTest.attr('disabled','disabled');
		}
	};

	Options.prototype._showInvalidQuery = function(msg){
		msg = msg || 'Invalid JQL query';

		this.$alert
			.addClass('alert-danger')
			.removeClass('alert-success')
			.show()
			.find('p').html('<b>Error:</b> ' + msg);

		this.$btnSave.attr('disabled','disabled');
	};

	Options.prototype._showSuccessAlert = function(msg){
		this.$alert
			.addClass('alert-success')
			.removeClass('alert-danger')
			.show()
			.find('p').html(msg);
	};

	window.Options = new Options();
})(jQuery)