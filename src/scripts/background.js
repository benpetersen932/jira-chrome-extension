/* global chrome */

var AppManager = function() {
	this._init();
};

AppManager.prototype = {
	_init:function(){
		this.JIRA_BASE_URL = 'https://jira.markit.com/';
		this.JIRA_SEARCH_URL = 'https://jira.markit.com/rest/api/2/search';
		this.PROJECTS_BASE_URL = 'https://projects.markit.com/';
		this.PROJECTS_SEARCH_URL = 'https://projects.markit.com/rest/api/2/search';
		chrome.browserAction.setBadgeBackgroundColor({ color:'#165788' });
	
		this.auth = false;
		this.issues = {
			jira:[],
			projects:[]
		};
		this.user = {
			jira:{},
			projects:{}
		};
	
		this._attachEvents();
	},
	
	_attachEvents:function(){
		chrome.browserAction.onClicked.addListener($.proxy(this._handleClickBrowserAction,this));
		chrome.notifications.onClicked.addListener($.proxy(this._handleClickNotification,this));
		chrome.runtime.onInstalled.addListener($.proxy(this._handleStartup,this));
		chrome.runtime.onStartup.addListener($.proxy(this._handleStartup,this));
		chrome.tabs.onActivated.addListener($.proxy(this._handleActivateTab,this));
	},
	
	_checkAuth:function(){
		var app = this,
		$auth = function(url){
			return $.ajax({
				beforeSend:function(xhr,settings){
					xhr.url = url;
				},
				context:app,
				dataType:'json',
				error:function(xhr, err, text){
					app.auth = false;
					app._handleApiError(xhr);
				},
				type:'get',
				url: url + 'rest/auth/1/session'
			});
		};
	
		$.when(
			$auth(this.JIRA_BASE_URL),
			$auth(this.PROJECTS_BASE_URL)
		)
		.then(function(jira, projects){
			app.auth = true;
	
			var user = app.storage.get('user');
	
			if(!user){
				app._getUser();
			}
			else {
				app.user = user;
			}
		});	
	},
	
	_getUser:function(){
		var $getUser = function(url){
			return $.ajax({
				beforeSend:function(xhr,settings){
					xhr.url = url;
				},
				context:this,
				dataType:'json',
				error:function(xhr, err, text){
					this._handleApiError(xhr);
				},
				type:'get',
				url: url + 'rest/gadget/1.0/currentUser'
			});
		};
	
		var app = this;
	
		$.when(
			$getUser(this.JIRA_BASE_URL),
			$getUser(this.PROJECTS_BASE_URL)
		)
		.then(function(jira, projects){
			app.user = {
				jira:jira[0],
				projects:projects[0]
			};
			app.storage.set('user', app.user);
		});	
	},
	
	_handleActivateTab: function(tab){
		if(this.auth){
			this._getIssuesForBrowserAction();
		}
		else{
			this._checkAuth();
		}
	},
	
	_handleClickBrowserAction: function(tab){
		if(this.auth){
			chrome.browserAction.setPopup({
				pop:chrome.extension.getURL('/page/popup.html')
			});
		}
		else{
			chrome.tabs.create({ url:this.JIRA_BASE_URL});
			chrome.tabs.create({ url:this.PROJECTS_BASE_URL });
			chrome.browserAction.setPopup({
				popup:''
			});
		}
	},
	
	_handleClickNotification: function(id) {
		if(id.indexOf('login_') > -1){
			var url = id.split('_')[1];
			chrome.tabs.create({ url:url });
		}
	},
	
	_handleApiError: function(xhr){
		var isAnonUser = (xhr.status === 400 && xhr.responseText.indexOf('anonymous users') > -1);
		console.log(xhr);

		if(xhr.status !== 0){
			chrome.browserAction.setBadgeText({ text:'' });
			chrome.browserAction.setPopup({ popup:'' });
		}
		
		//create an anchor in order to "dummy" a location object
		var temp = document.createElement('a');
			temp.href = xhr.url;
		
		if(xhr.status === 401 || isAnonUser){
			this.auth = false;
			
			chrome.notifications.create(
				'login_' + temp.origin + '_' + moment().format('MDYYYY'),
				{
					type: 'basic',
					title: 'Jira Issues',
					message: 'Please log in to ' + temp.origin,
					iconUrl: chrome.extension.getURL('images/jira.png')
				});
		}
		else if(xhr.status !== 0){
			chrome.notifications.create(
				'error_' + xhr.status + temp.origin + '_' + moment().format('MDYYYY'),
				{
					type: 'basic',
					title: temp.origin,
					message: xhr.status + ' ' + xhr.statusText + ' from ' + temp.origin,
					iconUrl: chrome.extension.getURL('images/jira.png')
				});
		}
	},
	
	/*
	 * previousVersion
	 * reason
	 */
	_handleStartup: function(data){
		console.log('startup:\n', arguments);
		if(data.reason === 'update'){
			// run update code
		}

		this._checkAuth();
	},
	
	_getIssuesForBrowserAction: function(){
		if(this.$xhr1){
			this.$xhr1.abort();
		}
	
		if(this.$xhr2){
			this.$xhr2.abort();
		}
	
		this.$xhr1 = this.$getCurrentIssues({ url: this.JIRA_SEARCH_URL });
		this.$xhr2 = this.$getCurrentIssues({ url: this.PROJECTS_SEARCH_URL	});
	
		var app = this;
	
		$.when(this.$xhr1, this.$xhr2)
			.then(function(xhr1,xhr2){
				var jira = xhr1[0],
					projects = xhr2[0],
					total = jira.total + projects.total;
	
				app.issues = {
					jira: jira.issues,
					jiraTotal: jira.total,
					projects: projects.issues,
					projectsTotal: projects.total
				};
	
				app.totals = {
					jira: jira.total,
					projects: projects.total
				};
	
				if(total > 0){
					chrome.browserAction.setBadgeText({ text:total.toString() });
					chrome.browserAction.setPopup({ popup:'pages/popup.html'});
				}
				else{
					chrome.browserAction.setBadgeText({ text:'' });
					chrome.browserAction.setPopup({ popup:'' });
				}
	
			});
	},
	
	$getCurrentIssues: function(options){
		var app = this,
			opts = $.extend(true, {
				beforeSend:function(xhr, settings){
					xhr.url = settings.url;
				},
				data:{
					fields: 'id,key,summary,comment,issuetype,created,assignee,status',
					jql: this.getQuery(),
					maxResults:10,
					startAt:0
				},
				error:function(xhr,err,status){
					app._handleApiError(xhr);
				},
				type:'get',
				url:this.JIRA_SEARCH_URL
			}, options);
		
		return $.ajax(opts);
	},
	
	getQuery: function() {
		return this.storage.get('query') || 'assignee=currentuser() AND resolution=unresolved';
	},
	
	guid: function() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
		}
	
		function guid() {
			return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
		}
	
		return guid();
	},
	
	storage: {
		_storage: window.localStorage,
	
		_clear: function() {
			this._storage.clear();
		},
		set: function(key, oVal) {
			var sVal = JSON.stringify(oVal);
	
			this._storage.setItem(key, sVal);
		},
		get: function(key) {
			var sVal = this._storage.getItem(key);
	
			return JSON.parse(sVal);
		},
		remove: function(key) {
			this._storage.removeItem(key);
		}
	}
};

(function(w) {
	w.app = new AppManager();
})(window);