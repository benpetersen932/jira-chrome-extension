# JIIA Chrome Extension

## Installation
1. Check out the project
2. Find all referernces of jira.markit.com and projects.markit.com and rename to your companies Jira board
3. Run 'npm install' from the CLI
4. Run 'gulp' from the CLI
5. Open Chrome and navigate to chrome://extensions
6. From the root of the project, drag the /build directory into the chrome://extensions window

## Screenshot
![My Assigned Jira Issues](https://lh3.googleusercontent.com/EKuv19tYnOLSE18lSn4swspTFcd3Y5CjDiFb8FiF2HGERLzW-DXPJSTHWHdQNujjVhD9zSCff8mUj9X2BqgSdi_FTY4OzJ2WDwZ70IcF7TVCP_DgAsXwFaWao3CKA9syrvAxEe-DmqNSvMVawwRqK_v4T3puOO9X-TfqQo6A-GQFAhjWRXS166miYSZYwxwyxhtM86pSWy7FJUdh_76GL20ZacXlRV3QGRnHxCOPsm4jp3GiZrO7s1gaEGwI-R59wdjDktBGkcBOMJO_jPT99arjl7zdWdMvCrKUEbljO1LmQ3vLGoHirQBlHnK9M8aAdaJxjzrG8p8XiXqi6s1nwBc5rYmyX5L6ciD2DcZl_K3KiLm57n8wG_wTEGO1ISJUyoZPaJ-bMStcZHYATNoK9xSfVB3Vq4trwXre5p28XVPAl7pDDDdh35F_VZkmkUNoT7fle_tIJ3BFxsjYdKIt2uyLvrqvoz8f2DOzcjVWSL3hGr30IbSabtECcF9yZvK9Kc6lyrVJ3-qVwkym8_JgqH9RWx1AjBaUP4O6OmQUhyG00DjQk6UPXaRYyeCzWMr31mCm4-JN-K3PpOQalOFAL_d1VRYanRlwc9pJYgzVL-iFStx-RM7iqBD4X3pX53jx0eqYjHWYLx0RVUtL-mSKjT9BlilFJj48yg=w306-h230-no)

## Documentation
1. dashboard.js - holds all factories for retrieving async data from the API
2. Jira.js - sets watchers
3. popup.js - assigns click events using jQuery and creates the modal 
4. options.js - allows custom JQL Queries to be used, must pass their requirements to be saved.
5. background.js - background processes and user authentication

## Notes
1. Active JIRA logins are required and a popup will appear until both are activated. My team uses jira.markit.com for production issues and projects.markit.com for projects. If you have different URLs, you'll need to manually edit them. 
2. Searching is not complete, this is an older app that I built and the API was discontinued. I may work on this in the future, but the main purpose is to view currently assigned stories, so when a team member assigns you a story you know about it right away.
3. When running npm install you'll notice a number of deprecated packages, but the app works as expected and upgrading will be time consuming. It's on my list to do before the app becomes un-usable because a package is no longer available.

### TODO
* Update Url system and provide users way to edit them
* Research Chrome Extension store and publish

